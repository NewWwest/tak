﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Tak.Data;
using Tak.Models;

namespace Tak.Services
{
    public class DataService
    {
        DataStorage data;

        public DataService(DataStorage data)
        {
            this.data = data;
        }

        //SERIALISATION
        static public Dictionary<string, List<DateTime>> Deserialize(IEnumerable<string> entries)
        {
            //Marcin : 4/6/2018 1:19:35 PM;
            IFormatProvider provider = new CultureInfo("us-US");
            var res = new Dictionary<string, List<DateTime>>();
            for (int i = 0; i < entries.Count(); i++)
            {

                try
                {
                    var entry = entries.ElementAt(i);
                    if (String.IsNullOrEmpty(entry))
                        continue;
                    var name = entry.Substring(0, entry.IndexOf(':'));
                    if (!res.ContainsKey(name))
                    {
                        res.Add(name, new List<DateTime>());
                    }
                    var dateValue = entry.Substring(entry.IndexOf(':') + 1);
                    dateValue = dateValue.TrimStart();
                    DateTime parsedDate;
                    parsedDate = DateTime.ParseExact(dateValue, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
                    res[name].Add(parsedDate);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
            return res;
        }


        //CREATION FUNC
        public bool AddUserBet(UserBet userBet)
        {
            if(data.UserBets.TryAdd(userBet.Username, (userBet.Bet,userBet.Time)))
            {
                data.TakTimes[userBet.Username] = new List<DateTime>();
                return true;
            }
            return false;
        }
        public bool AddTak(string username, DateTime time, out int TakCount)
        {
            try
            {
                data.TakTimes[username].Add(time);
                TakCount = data.TakTimes[username].Count;
                return true;
            }
            catch (Exception)
            {
                TakCount = -1;
                return false;
            }
        }


        //CHECKING FUNC
        public bool UserHasSetBet(string username)
        {
            return data.UserBets.ContainsKey(username);
        }
        public int GetBet(string username)
        {
            return data.UserBets[username].Item1;
        }
        public int GetTaks(string username)
        {
            return data.TakTimes[username].Count;
        }

        //RESULTS FUNC
        private IEnumerable<(string,int,DateTime)> GetUserBets()
        {
            return data.UserBets.Select(p => (p.Key, p.Value.Item1,p.Value.Item2)).ToList();
        }
        private IEnumerable<(string, int)> GetAllTaks()
        {
            return data.TakTimes.Select(p => (p.Key, p.Value.Count)).ToList();
        }
        private IEnumerable<(string, DateTime)> GetAllData()
        {
            var temp = new List<(string, DateTime)>();
            foreach (var user in data.TakTimes.Keys)
            {
                temp.AddRange(data.TakTimes[user].Select(date => (user, date)));
            }
            return temp;
        }

        
        //ADMINTOOLS
        public void Reset()
        {
            foreach (var taktime in data.TakTimes)
            {
                data.TakTimes.Remove(taktime.Key, out _);
            }
            foreach (var userbet in data.UserBets)
            {
                data.UserBets.Remove(userbet.Key, out _);
            }
            
        }
        public bool DeleteUser(string Username)
        {
            var bool1 = data.UserBets.Remove(Username, out _);
            var bool2 = data.TakTimes.Remove(Username, out _);
            return bool1 && bool2;
        }
        public bool AddTaks(string username, int count)
        {
            try
            {
                for (int i = 0; i < count; i++)
                {
                    data.TakTimes[username].Add(DateTime.Now);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string SerializeData()
        {
            try
            {
                StringBuilder str = new StringBuilder("");
                var bety = this.GetUserBets();
                if (bety.Count() == 0) return null;
                var taki = this.GetAllTaks();
                if (taki.Count() == 0) return null;
                var times = this.GetAllData();
                if (times.Count() == 0) return null;

                double avg;
                if (taki.Count() > 4)
                    avg = taki.OrderBy(t => t.Item2).Skip(1).Take(taki.Count() - 1).Average(t => t.Item2);
                else
                    avg = taki.Average(t => t.Item2);
                str.Append($"AVERAGE TAKS : {avg}\n");
                var min = bety.Min(si => Math.Abs(si.Item2 - avg));
                var winners = bety.Where(si => Math.Abs(si.Item2 - avg) == min);

                str.Append($"WINNERS :");
                foreach (var win in winners)
                {
                    str.Append($"  {win.Item1} (betted:{ win.Item2})");
                }


                str.Append("#\n");
                str.Append("#DATA\n");
                str.Append("#USER : USERS_BET : BET_TIME\n");
                foreach (var bet in bety)
                {
                    str.Append(bet.Item1 + " : " + bet.Item2 + " : " + bet.Item3 + ";\n");
                }
                str.Append("#\n");
                str.Append("#USER : TIMES_TAK\n");
                foreach (var tak in taki)
                {
                    str.Append(tak.Item1 + " : " + tak.Item2 + ";\n");
                }
                str.Append("#\n");
                str.Append("#USER : TIME_OF_TAK\n");
                foreach (var time in times)
                {
                    str.Append(time.Item1 + " : " + time.Item2 + ";\n");
                }
                return str.ToString();
            }
            catch (Exception)
            {

                return null;
            }
        }
        public SessionOverview GetSessionOverview(int id)
        {
            var bety = this.GetUserBets();
            if (bety.Count() == 0) return null;
            var taki = this.GetAllTaks();
            if (taki.Count() == 0) return null;
            var times = this.GetAllData();
            if (times.Count() == 0) return null;
            double avg;
            if (taki.Count() > 4)
                avg = taki.OrderBy(t => t.Item2).Skip(1).Take(taki.Count() - 1).Average(t => t.Item2);
            else
                avg = taki.Average(t => t.Item2);
            var min = bety.Min(si => Math.Abs(si.Item2 - avg));
            var winners = bety.Where(si => Math.Abs(si.Item2 - avg) == min);


            var ses = new SessionOverview();
            ses.ID = id;
            ses.MinTak = taki.Min(si => si.Item2);
            ses.AvgTak = (float)avg;
            ses.MaxTak = taki.Max(si => si.Item2);
            ses.Winners = winners.Select(si => si.Item1).ToArray();

            return ses;
        }
        public IEnumerable<(string, int, int)> UsernameBetTaks()
        {
            return data.TakTimes.Select(p => (p.Key,data.UserBets[p.Key].Item1, p.Value.Count)).ToList();
        }


        



    }
}
