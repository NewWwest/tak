﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Tak.Data;
using Tak.Models;

namespace Tak.Services
{
    public class UserService
    {
        DataStorage storage;
        public UserService(DataStorage storage)
        {
            this.storage = storage;
        }

        public int UserCount() => storage.Users.Count;

        public bool AddNewUser(string Username, string Password)
        {
            if (storage.Users.ContainsKey(Username))
                return false;
            try
            {
                var newUser = new User();
                newUser.IsAdmin = false;
                newUser.Username = Username;
                //STEP 1 Create the salt value with a cryptographic PRNG:

                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
                // STEP 2 Create the Rfc2898DeriveBytes and get the hash value:

                var pbkdf2 = new Rfc2898DeriveBytes(Password, salt, 10000);
                byte[] hash = pbkdf2.GetBytes(20);
                //  STEP 3 Combine the salt and password bytes for later use:

                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);
                //  STEP 4 Turn the combined salt+hash into a string for storage


                newUser.PasswordHash = Convert.ToBase64String(hashBytes);
                storage.Users.AddOrUpdate(newUser.Username, newUser, (s, u) => { throw new Exception(); });
                storage.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

            
        }

        public User GetUser(string username)
        {
            try
            {
                return storage.Users[username];
            }
            catch
            {
                return null;
            }
        }

        public bool VerifyPassword(string username, string password)
        {
            try
            {
                var user = storage.Users[username];
                /* Fetch the stored value */
                string savedPasswordHash = user.PasswordHash;
                /* Extract the bytes */
                byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
                /* Get the salt */
                byte[] salt = new byte[16];
                Array.Copy(hashBytes, 0, salt, 0, 16);
                /* Compute the hash on the password the user entered */
                var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
                byte[] hash = pbkdf2.GetBytes(20);
                /* Compare the results */
                for (int i = 0; i < 20; i++)
                    if (hashBytes[i + 16] != hash[i])
                        return false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsAdmin(string username)
        {
            try
            {
                return storage.Users[username].IsAdmin;
            }
            catch
            {
                return false;
            }
        }

        public bool ChangePassword(string Username, string OldPassword, string Password, string PasswordVer)
        {
            try
            {
                var user = storage.Users[Username];
                byte[] salt;
                new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
                // STEP 2 Create the Rfc2898DeriveBytes and get the hash value:

                var pbkdf2 = new Rfc2898DeriveBytes(Password, salt, 10000);
                byte[] hash = pbkdf2.GetBytes(20);
                //  STEP 3 Combine the salt and password bytes for later use:

                byte[] hashBytes = new byte[36];
                Array.Copy(salt, 0, hashBytes, 0, 16);
                Array.Copy(hash, 0, hashBytes, 16, 20);
                //  STEP 4 Turn the combined salt+hash into a string for storage


                user.PasswordHash = Convert.ToBase64String(hashBytes);
                storage.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        

    }
}
