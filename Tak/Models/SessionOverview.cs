﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tak.Models
{
    public class SessionOverview
    {
        public int ID { get; set; }

        public int MaxTak { get; set; }

        public int MinTak { get; set; }

        public float AvgTak { get; set; }

        public string[] Winners { get; set; }
    }
}
