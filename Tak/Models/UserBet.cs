﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tak.Models
{
    public class UserBet
    {
        [Required]
        [Display(Name = "TakName", Description ="UserName")]
        [RegularExpression("[0-9a-zA-Z]+")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "TakBet")]
        [Range(0,Int32.MaxValue)]
        public int Bet { get; set; }

        public DateTime Time { get; set; }



    }
}
