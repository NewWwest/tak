﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tak.Models;
using Tak.Services;

namespace Tak.Controllers
{
    public class HomeController : Controller
    {
        DataService data;

        public HomeController(DataService serv)
        {
            data = serv;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(UserBet userBet)
        {
            return View(userBet);
        }

        public IActionResult CheckIn([FromForm()]UserBet userBet)
        {
            userBet.Time = DateTime.Now;
            if (!ModelState.IsValid || userBet.Username.Contains(':'))
            {
                TempData["Warning"] = "Invalid data";
                return RedirectToAction(nameof(Login),userBet);
            }
            if(!data.AddUserBet(userBet))
            {
                TempData["Warning"] = "Username taken";
                return RedirectToAction(nameof(Login), userBet);
            }
            HttpContext.Response.Cookies.Append("clicks", "0");
            HttpContext.Response.Cookies.Append("username", userBet.Username);
            HttpContext.Response.Cookies.Append("bet", userBet.Bet.ToString());
            TempData["username"] = userBet.Username;
            TempData["bet"] = userBet.Bet;
            return RedirectToAction(actionName:nameof(Betting),routeValues: new { id = userBet.Username });
        }

        public IActionResult Betting(string id)
        {
            if(String.IsNullOrEmpty(id))
            {
                TempData["Warning"] = "No Username";
                return RedirectToAction(nameof(Index));
            }
            if (data.UserHasSetBet(id))
            {
                int bet = data.GetBet(id);
                int taks = data.GetTaks(id);
                HttpContext.Response.Cookies.Append("clicks", taks.ToString());
                HttpContext.Response.Cookies.Append("username", id);
                HttpContext.Response.Cookies.Append("bet", bet.ToString());
                TempData["username"] = id;
                TempData["bet"] = bet;

                return View();
            }
            else
            {
                TempData["Warning"] = "Please Register first";
                return RedirectToAction(nameof(Login));
            }
        }

        [HttpPost]
        public IActionResult ReceiveTak(string time, string username)
        {
            if(!Int64.TryParse(time,out long msSinceEpoch))
                return new StatusCodeResult(StatusCodes.Status406NotAcceptable);
            var date= new DateTime(1970, 1, 1) + new TimeSpan(msSinceEpoch * 10000);

            if (!data.AddTak(username, date, out int count))
            {
                if(count==-1)
                    return new JsonResult(new { count = -1 });
                else
                    return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            return new JsonResult(new { count = count });
        }

        public IActionResult ChangeLog()
        {
            return View();
        }
        








        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
