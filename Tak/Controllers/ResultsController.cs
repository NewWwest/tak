﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tak.Models;
using Tak.Services;

namespace Tak.Controllers
{
    public class ResultsController : Controller
    {
        DataService data;
        readonly string scorePath = PathConstructer.MakePath("Results/");
        public ResultsController(DataService data)
        {
            this.data = data;
        }

        public IActionResult Index()
        {
            List<SessionOverview> list = new List<SessionOverview>();

            XmlSerializer xmlSerializer = new XmlSerializer(list.GetType());

            using (StreamReader textWriter = new StreamReader(scorePath+"ScoreBoard"))
            {
                list=(List<SessionOverview>)xmlSerializer.Deserialize(textWriter);
            }
            return View(list);
        }

        public async Task<IActionResult> TakPerMinute(string id)
        {
            //#USER : TIME_OF_TAK

            try
            {
                string data = "";
                using (var file = new StreamReader(scorePath + "Results" + id.ToString()))
                {
                    data = await file.ReadToEndAsync();
                }
                var lines = data.Split(';','\n');
                int start = 0;
                bool valid = false;
                for (int i = 0; i < lines.Length; i++)
                {
                    if(lines[i]== "#USER : TIME_OF_TAK")
                    {
                        start = i + 1;
                        valid = true;
                        break;
                    }
                }
                if (!valid)
                    throw new Exception();
                var res = DataService.Deserialize(lines.Skip(start));

                var dates = res.SelectMany(kv => kv.Value);
                var lowDate = dates.Min();
                var TimeSpans = dates.Select(d => (int)(d - lowDate).TotalMinutes);
                var Total = TimeSpans.GroupBy(s => s).Select(g => (g.Key, g.Count()));
                var highminute = TimeSpans.Max();
                List<int> xd = new List<int>(new int[highminute + 1]);
                foreach (var item in Total)
                {
                    xd[item.Item1] = item.Item2;
                }
                StringBuilder TotalForJS = new StringBuilder("[");
                //{ x: '2014-06-11', y: 10},
                for (int minute = 0; minute < xd.Count-1; minute++)
                {
                    TotalForJS.Append("{ x: " + minute.ToString() + " , y: " + xd[minute] + "},");
                }
                TotalForJS.Append("{ x: " + (xd.Count - 1).ToString() + " , y: " + xd[xd.Count - 1] + "}]");



                return View(nameof(TakPerMinute),model:TotalForJS.ToString());
            }
            catch (Exception)
            {
                TempData["Warning"] = "Something went wrong";
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> TakPerMinuteTest(string id)
        {
            //#USER : TIME_OF_TAK

            try
            {
                string data = "";
                using (var file = new StreamReader(scorePath + "Results" + id.ToString()))
                {
                    data = await file.ReadToEndAsync();
                }
                var lines = data.Split(';', '\n');
                int start = 0;
                bool valid = false;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i] == "#USER : TIME_OF_TAK")
                    {
                        start = i + 1;
                        valid = true;
                        break;
                    }
                }
                if (!valid)
                    throw new Exception();
                var res = DataService.Deserialize(lines.Skip(start));

                var dates = res.SelectMany(kv => kv.Value);
                var lowDate = dates.Min();
                var TimeSpans = dates.Select(d => (int)(d - lowDate).TotalMinutes);
                var Total = TimeSpans.GroupBy(s => s).Select(g => (g.Key, g.Count()));
                var highminute = TimeSpans.Max();
                List<int> xd = new List<int>(new int[highminute + 1]);
                foreach (var item in Total)
                {
                    xd[item.Item1] = item.Item2;
                }
                StringBuilder TotalForJS = new StringBuilder("[\n");
                //{ x: '2014-06-11', y: 10},
                for (int minute = 0; minute < xd.Count - 1; minute++)
                {
                    TotalForJS.Append("{ x: " + minute.ToString() + " , y: " + xd[minute] + "},\n");
                }
                TotalForJS.Append("{ x: " + (xd.Count - 1).ToString() + " , y: " + xd[xd.Count - 1] + "}\n]");



                return Content(TotalForJS.ToString());
            }
            catch (Exception)
            {
                TempData["Warning"] = "Something went wrong";
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> Raw(int id)
        {
            try
            {
                using (var file = new StreamReader(scorePath+"Results" + id.ToString()))
                {
                    return Content(await file.ReadToEndAsync());
                }
            }
            catch (Exception)
            {
                TempData["Warning"] = "There is no results for " + id.ToString() + "'s lecture";
                return RedirectToAction(nameof(Index)); 
            }
            
        }
    }
}