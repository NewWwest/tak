﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tak.Models;
using Tak.Services;

namespace Tak.Controllers
{
    public class AdminToolsController : Controller
    {
        DataService data;
        UserService users;
        readonly string scoresFolder = PathConstructer.MakePath("Results/");

        public AdminToolsController(DataService serv, UserService users)
        {
            data = serv;
            this.users = users;
        }

        public IActionResult Index()
        {
            return new StatusCodeResult(StatusCodes.Status404NotFound);
        }

        [HttpGet]
        public IActionResult Results()
        {
            ViewData["Action"] = "Results";
            ViewData["Controller"]= "AdminTools";
            return View("Authorize");

        }
        [HttpPost]
        public IActionResult Results([FromForm]string Username, [FromForm]string Password)
        {

            if (String.IsNullOrEmpty(Username) || String.IsNullOrEmpty(Password) ||
               !users.IsAdmin(Username) || !users.VerifyPassword(Username, Password))
                return new StatusCodeResult(StatusCodes.Status403Forbidden);

            string str = data.SerializeData();
            if (str == null)
                return new StatusCodeResult(StatusCodes.Status404NotFound);

            return Content(str);
        }

        [HttpGet]
        public IActionResult UserBetsCRUD()
        {
            ViewData["Action"] = "UserBetsCRUD";
            ViewData["Controller"] = "AdminTools";
            return View("Authorize");
        }
        [HttpGet]
        public IActionResult UserBetsCRUDDelete()
        {
            return RedirectToAction("UserBetsCRUD");
        }

        [HttpPost]
        public IActionResult UserBetsCRUD([FromForm]string username, [FromForm]string password)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) ||
               !users.IsAdmin(username) || !users.VerifyPassword(username, password))
            {
                TempData["Warning"] = "Invalid username/password";
                return View();
            }

            var UsernameBetTaks = data.UsernameBetTaks();

            return View(UsernameBetTaks);
        }

        [HttpPost]
        public IActionResult UserBetsCRUDDelete([FromForm]string username, [FromForm]string password, [FromForm]string Betname)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) ||
               !users.IsAdmin(username) || !users.VerifyPassword(username, password))
            {
                TempData["Warning"] = "Invalid username/password";
                return View("UserBetsCRUD");
            }

            var result = data.DeleteUser(Betname);
            if(result)
                TempData["Warning"] = "Operation succeded";
            else
                TempData["Warning"] = "Invalid data";

            var UsernameBetTaks = data.UsernameBetTaks();
            return View("UserBetsCRUD",UsernameBetTaks);

        }

       

        [HttpGet]
        public IActionResult AddTaksToUser()
        {
            return View(nameof(AddTaksToUser), ("",0));
        }

        [HttpPost]
        public IActionResult AddTaksToUser([FromForm] string Rootname, [FromForm] string password, [FromForm] string username, [FromForm] int TaksToAdd)
        {
            if (String.IsNullOrEmpty(Rootname) || String.IsNullOrEmpty(password) ||
               !users.IsAdmin(Rootname) || !users.VerifyPassword(Rootname, password))
                return new StatusCodeResult(StatusCodes.Status403Forbidden);

            if (data.AddTaks(username,TaksToAdd))
            {
                TempData["Warning"] = "Operation succeded";
                return View(nameof(AddTaksToUser), ("",0));
            }
            else
            {
                TempData["Warning"] = "Operation did NOT succeded";
                return View(nameof(AddTaksToUser), (username,TaksToAdd));
            }

        }


        [HttpGet]
        public IActionResult EndSession()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> EndSession([FromForm] string username, [FromForm] string password, [FromForm] int number,[FromForm] string Reset)
        {
            if(String.IsNullOrEmpty(username)||String.IsNullOrEmpty(password) || 
                !users.IsAdmin(username) || !users.VerifyPassword(username, password))
            {
                TempData["Warning"] = "Invalid username/password";
                return View();
            }
            if(System.IO.File.Exists(scoresFolder + "Results" + number.ToString()))
            {
                TempData["Warning"] = "Invalid Lecture number";
                return View();
            }

            try
            {
                using (var file = new StreamWriter(scoresFolder+ "Results" + number.ToString()))
                {
                    await file.WriteAsync(data.SerializeData());
                    var session = data.GetSessionOverview(number);
                    if(Reset.Equals("Reset", StringComparison.InvariantCultureIgnoreCase))
                        data.Reset();



                    List<SessionOverview> list = new List<SessionOverview>();
                    XmlSerializer xmlSerializer = new XmlSerializer(list.GetType());
                    using (StreamReader textReader = new StreamReader(scoresFolder+"ScoreBoard"))
                    {
                        list = (List<SessionOverview>)xmlSerializer.Deserialize(textReader);
                    }
                    list.Add(session);
                    using (StreamWriter textwriter = new StreamWriter(scoresFolder+"ScoreBoard"))
                    {
                        xmlSerializer.Serialize(textwriter,list);
                    }



                    TempData["Warning"] = "Operation Succesfull";
                    return RedirectToAction(nameof(Index), controllerName: "Home");
                }


            }
            catch (Exception)
            {
                TempData["Warning"] = "Unexpected error";
                return RedirectToAction(nameof(Index),controllerName:"Home");
            }


        }


        /*[HttpGet]
       public IActionResult DeleteUser()
       {
           return View(nameof(DeleteUser),"");

       }
       [HttpPost]
       public IActionResult DeleteUser([FromForm] string Rootname, [FromForm] string password, [FromForm] string username)
       {
           if (String.IsNullOrEmpty(Rootname) || String.IsNullOrEmpty(password) ||
              !users.IsAdmin(Rootname) || !users.VerifyPassword(Rootname, password))
               return new StatusCodeResult(StatusCodes.Status403Forbidden);

           if (data.DeleteUser(username))
           {
               TempData["Warning"] = "Operation succeded";
               return View(nameof(DeleteUser), "");
           }
           else
           {
               TempData["Warning"] = "Operation did NOT succeded";
               return View(nameof(DeleteUser), username);
           }

       }*/
    }
}