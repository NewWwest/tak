﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tak.Models;
using Tak.Services;

namespace Tak.Controllers
{
    public class UsersController : Controller
    {
        UserService userdata;
        public UsersController(UserService userdata)
        {
            this.userdata = userdata;
        }

        public ActionResult Index()
        {
            return View(nameof(Index), userdata.UserCount());
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([FromForm] string Username, [FromForm] string Password)
        {
            if(String.IsNullOrEmpty(Username)|| String.IsNullOrEmpty(Password))
            {
                TempData["Warning"] = "Invalid data";
                return View();
            }

            var result=userdata.AddNewUser(Username, Password);
            if (!result)
            {
                TempData["Warning"] = "Username  taken";
                return View();
            }

            TempData["Warning"] = "Operation succesful";
            return RedirectToAction("Index", controllerName: "Home");
        }


        public ActionResult ChangePassword()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword([FromForm] string Username, [FromForm] string OldPassword, 
                                            [FromForm] string Password, [FromForm] string PasswordVer)
        {
            if(Password!=PasswordVer)
            {
                TempData["Warning"] = "Passwords missmatch";
                return View();
            }
            if(userdata.GetUser(Username)==null)
            {
                TempData["Warning"] = "No such user";
                return View();
            }
            if (!userdata.VerifyPassword(Username, OldPassword))
            {
                TempData["Warning"] = "Bad Old Password";
                return View();
            }
            var result = userdata.ChangePassword(Username, OldPassword, Password, PasswordVer);
            if(!result)
            {
                TempData["Warning"] = "Error Occured. Try again";
                return View();
            }



            TempData["Warning"] = "operation succesfull";
            return RedirectToAction("Index", controllerName: "Home");
        }



       /* // GET: Users/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Users/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/
    }
}