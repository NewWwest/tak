﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Tak.Models;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using Tak.Services;

namespace Tak.Data
{
     public class DataStorage
    {
        public readonly ConcurrentDictionary<string, User> Users = new ConcurrentDictionary<string, User>();
        public readonly  ConcurrentDictionary<string, (int, DateTime)> UserBets=new ConcurrentDictionary<string, (int, DateTime)>();
        public readonly  ConcurrentDictionary<string, List<DateTime>> TakTimes = new ConcurrentDictionary<string, List<DateTime>>();
        private readonly string UsersPath = PathConstructer.MakePath("Data/Users");

        public DataStorage()
        {
            try
            {
                using (var file = new StreamReader(UsersPath))
                {
                    var UsersList = new List<User>();
                    XmlSerializer xmlSerializer = new XmlSerializer(UsersList.GetType());
                    UsersList = (List<User>)xmlSerializer.Deserialize(file);
                    foreach (var user in UsersList)
                    {
                        Users.AddOrUpdate(user.Username, user, (s, u) => throw new Exception("Dictionary Should Be Empty"));
                    }
                }
            }
            catch
            {
                CreateRoot();
                SaveChanges();
            }
        }

        public void SaveChanges()
        {
            var UsersList = Users.Values.ToList();
            XmlSerializer xmlSerializer = new XmlSerializer(UsersList.GetType());
            using (StreamWriter textwriter = new StreamWriter(UsersPath))
            {
                xmlSerializer.Serialize(textwriter, UsersList);
            }
        }

        private void CreateRoot()
        {

            var root = new User();
            root.IsAdmin = true;
            root.Username = "root";
            //STEP 1 Create the salt value with a cryptographic PRNG:

            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            // STEP 2 Create the Rfc2898DeriveBytes and get the hash value:

            var pbkdf2 = new Rfc2898DeriveBytes("ITakZarazZmienie", salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            //  STEP 3 Combine the salt and password bytes for later use:

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            //  STEP 4 Turn the combined salt+hash into a string for storage


            root.PasswordHash = Convert.ToBase64String(hashBytes);
            Users.AddOrUpdate(root.Username, root, (s, u) => { throw new Exception("Root Was already created"); });
        }


    }
}
